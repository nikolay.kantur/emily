import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:emily/classes/track.class.dart';
import 'package:emily/pages/player/player.page.dart';
import 'package:emily/resources/colors.dart';
import 'package:emily/resources/resources.dart';
import 'package:emily/services/player.service.dart';
import 'package:rxdart/rxdart.dart';

class PlayerPanel extends StatefulWidget {
  const PlayerPanel({Key? key}) : super(key: key);

  @override
  _PlayerPanelState createState() => _PlayerPanelState();
}

class _PlayerPanelState extends State<PlayerPanel> {
  final Image noCover =
      const Image(image: AssetImage(AppImages.noCover), fit: BoxFit.cover);
  PlayerService playerService = PlayerService();
  Track? track;

  double progressValue = 0;
  int durationInMilliseconds = 1; // should not be == 0
  int positionInMilliseconds = 0;

  late Image coverImage = noCover;
  final PublishSubject<void> unsubscriber = PublishSubject<void>();

  final double sizePanel = 64;
  final double sizeProgress = 5;

  late Icon loopModeIcon;

  @override
  void initState() {
    playerService.playerState$.takeUntil(unsubscriber).listen((_) {
      setState(() {});
    });

    playerService.track$.takeUntil(unsubscriber).listen((track) {
      this.track = track;
      coverImage = track?.cover != null
          ? Image.memory(
              track!.cover!,
              fit: BoxFit.cover,
            )
          : noCover;
      setState(() {});
    });

    playerService.duration$
        .takeUntil(unsubscriber)
        .listen((Duration? duration) {
      if (duration == null) return;
      durationInMilliseconds =
          duration.inMilliseconds != 0 ? duration.inMilliseconds : 1;
      setProgressValue();
    });

    playerService.position$.takeUntil(unsubscriber).listen((Duration position) {
      positionInMilliseconds = position.inMilliseconds;
      setProgressValue();
    });

    playerService.loopMode$.takeUntil(unsubscriber).listen((_) {
      setState(() {});
    });

    playerService.shuffleMode$.takeUntil(unsubscriber).listen((_) {
      setState(() {});
    });

    super.initState();
  }

  setProgressValue() {
    progressValue = positionInMilliseconds / durationInMilliseconds;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: Colors.blue,
      child: SizedBox(
        height: sizePanel + sizeProgress,
        child: Column(
          children: [
            LinearProgressIndicator(
              value: progressValue,
              minHeight: sizeProgress,
              color: Colors.blue,
              backgroundColor: Colors.white70,
            ),
            Row(
              children: [
                _CoverWidget(coverImage: coverImage, size: sizePanel),
                TrackNameWidget(track: track),
                // SkipToPrevButton(),
                SkipToNextButton(),
                // NextShuffleModeButton(),
                // LoopModeButton(),
                PlayPauseButton(),
                // StopButton(),
                const SizedBox(width: 8),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  dispose() {
    unsubscriber.add(null);
    super.dispose();
  }
}

class NextShuffleModeButton extends StatelessWidget {
  NextShuffleModeButton({
    Key? key,
  }) : super(key: key);

  final PlayerService playerService = PlayerService();

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: playerService.nextShuffleMode,
      icon: Icon(
        Icons.shuffle,
        color: playerService.shuffleMode ? Colors.black : AppColors.inactive,
      ),
      iconSize: 32,
      splashRadius: 28,
    );
  }
}

class SkipToPrevButton extends StatelessWidget {
  SkipToPrevButton({
    Key? key,
  }) : super(key: key);

  final PlayerService playerService = PlayerService();

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: playerService.skipToPrevious,
      icon: const Icon(Icons.skip_previous),
      iconSize: 32,
      splashRadius: 28,
    );
  }
}

class SkipToNextButton extends StatelessWidget {
  SkipToNextButton({
    Key? key,
  }) : super(key: key);

  final PlayerService playerService = PlayerService();

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: playerService.skipToNext,
      icon: const Icon(Icons.skip_next),
      iconSize: 32,
      splashRadius: 28,
    );
  }
}

class LoopModeButton extends StatelessWidget {
  LoopModeButton({
    Key? key,
  }) : super(key: key);

  final PlayerService playerService = PlayerService();

  Icon getLoopModeIcon(loopMode) {
    switch (loopMode) {
      case LoopMode.all:
        return const Icon(Icons.repeat);
      case LoopMode.one:
        return const Icon(Icons.repeat_one);
      default:
        return const Icon(Icons.repeat, color: AppColors.inactive);
    }
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: playerService.nextLoopMode,
      icon: getLoopModeIcon(playerService.loopMode),
      iconSize: 32,
      splashRadius: 28,
    );
  }
}

class StopButton extends StatelessWidget {
  StopButton({
    Key? key,
  }) : super(key: key);

  final PlayerService playerService = PlayerService();

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: playerService.stop,
      icon: const Icon(Icons.stop),
      iconSize: 32,
      splashRadius: 28,
    );
  }
}

class PlayPauseButton extends StatelessWidget {
  PlayPauseButton({
    Key? key,
  }) : super(key: key);

  final PlayerService playerService = PlayerService();

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: playerService.playPause,
      icon: (playerService.isPlaying == true)
          ? const Icon(Icons.pause)
          : const Icon(Icons.play_arrow_sharp),
      iconSize: 32,
      splashRadius: 28,
    );
  }
}

class TrackNameWidget extends StatelessWidget {
  final int maxLines;
  final CrossAxisAlignment crossAxisAlignment;
  const TrackNameWidget({
    Key? key,
    this.maxLines = 1,
    this.crossAxisAlignment = CrossAxisAlignment.start,
    required this.track,
  }) : super(key: key);

  final Track? track;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: track?.hasArtistAndTitle == true
                ? _ArtistTitleWidget(
                    track: track,
                    crossAxisAlignment: crossAxisAlignment,
                    maxLines: maxLines,
                  )
                : _BasenameWidget(
                    track: track,
                    crossAxisAlignment: crossAxisAlignment,
                    maxLines: maxLines,
                  )),
      ),
    );
  }
}

class _BasenameWidget extends StatelessWidget {
  final CrossAxisAlignment crossAxisAlignment;
  final int maxLines;
  const _BasenameWidget({
    Key? key,
    this.maxLines = 1,
    this.crossAxisAlignment = CrossAxisAlignment.start,
    required this.track,
  }) : super(key: key);

  final Track? track;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: crossAxisAlignment,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          track?.basename ?? '',
          // track!.basename,
          style: const TextStyle(fontSize: 22),
          overflow: TextOverflow.ellipsis,
          maxLines: maxLines, textAlign: TextAlign.center,
        ),
      ],
    );
  }
}

class _ArtistTitleWidget extends StatelessWidget {
  final CrossAxisAlignment crossAxisAlignment;
  final int maxLines;
  const _ArtistTitleWidget({
    Key? key,
    this.crossAxisAlignment = CrossAxisAlignment.start,
    this.maxLines = 1,
    required this.track,
  }) : super(key: key);

  final Track? track;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: crossAxisAlignment,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          track?.artist ?? 'Unknown',
          style: const TextStyle(fontSize: 22),
          overflow: TextOverflow.ellipsis,
          maxLines: maxLines,
        ),
        Text(
          track?.title ?? 'Unknown',
          style: const TextStyle(fontSize: 18),
          overflow: TextOverflow.ellipsis,
          maxLines: maxLines,
        ),
      ],
    );
  }
}

class _CoverWidget extends StatelessWidget {
  const _CoverWidget({
    Key? key,
    required this.coverImage,
    required this.size,
  }) : super(key: key);

  final Image coverImage;
  final double size;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: SizedBox(
        height: size,
        width: size,
        child: coverImage,
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (_) => const PlayerPage()),
        );
      },
    );
  }
}
