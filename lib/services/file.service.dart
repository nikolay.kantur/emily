import 'dart:io';

import 'package:emily/pages/filepicker/directory_preview_model.dart';
import 'package:emily/pages/filepicker/file_preview_model.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

class FileService {
  static const String defaultPath = '/storage/emulated/0';

  Future<DirectoryPreview> getDirectoryPreview(
    String path, {
    List<String> extensions = const [],
    bool onlyFiles = false,
    bool backFolder = true,
    OrderBy? orderBy = OrderBy.name,
    OrderType? orderType = OrderType.asc,
    bool recursive = false,
  }) async {
    if (await Permission.storage.isDenied) {
      var status = await Permission.storage.request();
      print('status: $status');
      if (status != PermissionStatus.granted) {
        return DirectoryPreview(
            path: path, fileList: [], error: 'Permission not granted');
      }
    }
    List<Directory>? rootList = await getExternalStorageDirectories();
    if (rootList == null) throw Exception('no external storage devices!');
    rootList = rootList
        .map((root) => root.parent.parent.parent.parent)
        .toList(); // ! ugly code
    print(rootList);

    final bool pathExists =
        rootList.where((root) => path.startsWith(root.path)).isNotEmpty;

    if (!pathExists) {
      return DirectoryPreview(
          path: '/',
          fileList: rootList
              .map((root) => root.path == defaultPath
                  ? FilePreview(
                      filename: 'Main',
                      path: root.path,
                      type: FileType.internal,
                    )
                  : FilePreview(
                      filename: 'SD card',
                      path: root.path,
                      type: FileType.sdCard,
                    ))
              .toList());
    }

    Directory dir = Directory(path);
    if (!dir.existsSync()) {
      dir = Directory(defaultPath);
    }
    if (!dir.existsSync()) {
      return DirectoryPreview(
          path: path,
          fileList: [],
          error: 'Path not exists',
          root:
              rootList.where((root) => path.startsWith(root.path)).first.path);
    }

    List<FileSystemEntity> listSync =
        dir.listSync(recursive: recursive, followLinks: true);

    List<FilePreview> folderList = onlyFiles ? [] : handleFolders(listSync);

    List<FilePreview> fileList = handleFiles(listSync);
    if (extensions.isNotEmpty) {
      fileList = fileList
          .where((file) => extensions.contains(file.extension))
          .toList();
    }

    switch (orderBy) {
      case OrderBy.name:
        fileList.sort(sortByAlphabet);
        folderList.sort(sortByAlphabet);
        break;
      case OrderBy.extension:
        fileList.sort(sortByExtension);
        folderList.sort(sortByAlphabet);
        break;
      default:
    }

    if (orderType == OrderType.desc) {
      fileList = fileList.reversed.toList();
      folderList = folderList.reversed.toList();
    }

    return DirectoryPreview(
        path: path,
        fileList: [
          ...((onlyFiles != true && backFolder == true)
              ? [getBackFolder(dir)]
              : []),
          ...folderList,
          ...fileList,
        ],
        root: rootList.where((root) => path.startsWith(root.path)).first.path);
  }

  List<FilePreview> handleFiles(List<FileSystemEntity> listSync) {
    return listSync
        .whereType<File>()
        .map((file) => FilePreview(
              filename: p.basename(file.path),
              path: file.path,
              extension: p.extension(file.path),
              type: FileType.file,
            ))
        .toList();
  }

  List<FilePreview> handleFolders(List<FileSystemEntity> listSync) {
    return listSync
        .whereType<Directory>()
        .map((folder) => FilePreview(
              filename: p.basename(folder.path),
              path: folder.path,
              type: FileType.directory,
            ))
        .toList();
  }

  FilePreview getBackFolder(Directory dir) {
    return FilePreview(
      filename: '..',
      path: dir.parent.path,
      extension: null,
      type: FileType.back,
    );
  }
}

enum OrderBy { name, extension }
enum OrderType { asc, desc }

int sortByAlphabet(FilePreview a, FilePreview b) {
  return a.filename.toLowerCase().compareTo(b.filename.toLowerCase());
}

/// Sorts alphabeticaly but via extension first
///
/// __ONLY FOR__ [type] == FileType.file !!!!
int sortByExtension(FilePreview a, FilePreview b) {
  int compareByExtension =
      a.extension!.toLowerCase().compareTo(b.extension!.toLowerCase());
  print(
      'compareByExtension: $compareByExtension, ${a.extension}, ${b.extension} ');
  if (compareByExtension != 0) return compareByExtension;
  return a.filename.toLowerCase().compareTo(b.filename.toLowerCase());
}

// ignore: provide_deprecation_message
@deprecated
int sortViaTypeAndAlphabet(FilePreview a, FilePreview b) {
  if (a.type == FileType.back) return -1;
  if (b.type == FileType.back) return 1;
  if (a.type == b.type) {
    return a.filename.toLowerCase().compareTo(b.filename.toLowerCase());
  }
  return a.type == FileType.file ? 1 : -1;
}
