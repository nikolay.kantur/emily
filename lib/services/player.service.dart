import 'package:just_audio/just_audio.dart';
import 'package:emily/classes/track.class.dart';
import 'package:emily/services/storage.service.dart';
import 'package:rxdart/subjects.dart';

class PlayerService {
  static final PlayerService _singleton = PlayerService._internal();

  factory PlayerService() {
    return _singleton;
  }

  // subscriptions & init
  PlayerService._internal() {
    _player.setLoopMode(LoopMode.off);

    _processingState$.listen((ProcessingState processingState) async {
      if (processingState == ProcessingState.completed) _onTrackCompleted();
    });

    shuffleMode$.listen((shuffleMode) {
      if (shuffleMode) _shuffleTracks();
    });

    _init();
  }

  // Init stuff
  void _init() async {
    await _loadLoopMode();
    await _loadShuffleMode();
  }

  // Services
  final StorageService storageServise = StorageService();

  // Player stuff
  final AudioPlayer _player = AudioPlayer();

  get isPlaying => _player.playerState.playing;
  Stream<PlayerState> get playerState$ => _player.playerStateStream;

  Stream<Duration?> get duration$ => _player.durationStream;
  Stream<Duration> get position$ => _player.positionStream;
  Future<void> setDuration(Duration duration) async {
    await _player.seek(duration);
  }

  Stream<ProcessingState> get _processingState$ =>
      _player.processingStateStream;

  // Tracks stuff
  final BehaviorSubject<List<Track>> _tracks = BehaviorSubject.seeded([]);
  List<Track> _tracksShuffled = [];
  List<Track> get _tracksCSM => shuffleMode ? _tracksShuffled : _tracks.value;

  // Track stuff
  final BehaviorSubject<Track?> _track = BehaviorSubject.seeded(null);
  Stream<Track?> get track$ => _track.stream;
  Track? get track => _track.value;

  bool get _isLastTrackCSM => _tracksCSM.isNotEmpty && _tracksCSM.last == track;
  bool get _isFirstTrackCSM =>
      _tracksCSM.isNotEmpty && _tracksCSM.first == track;
  bool get _isPenultTrackCSM =>
      _tracksCSM.length >= 2 && track == _tracksCSM[_tracksCSM.length - 2];
  Track get _nextTrackCSM {
    if (track == null) return _tracksCSM.first;
    if (_isLastTrackCSM) return _tracksCSM.first;

    int nextIndex = _tracksCSM.indexOf(track!) + 1;
    return _tracksCSM[nextIndex];
  }

  Track get _prevTrackCSM {
    if (track == null) return _tracksCSM.last;
    if (_isFirstTrackCSM) return _tracksCSM.last;

    int prevIndex = _tracksCSM.indexOf(track!) - 1;
    return _tracksCSM[prevIndex];
  }

  // LoopMode stuff
  final BehaviorSubject<LoopMode> _loopModeBS =
      BehaviorSubject.seeded(LoopMode.off);
  Stream<LoopMode> get loopMode$ => _loopModeBS.stream;
  LoopMode get loopMode => _loopModeBS.value;

  Future<void> _setLoopMode(LoopMode loopMode, {bool save = true}) async {
    _loopModeBS.add(loopMode);
    if (save) {
      String loopModeString = loopMode.toString().split('.').last;
      await _saveLoopMode(loopModeString);
    }
  }

  void nextLoopMode() {
    switch (loopMode) {
      case LoopMode.off:
        _setLoopMode(LoopMode.all);
        break;
      case LoopMode.all:
        _setLoopMode(LoopMode.one);
        break;
      case LoopMode.one:
        _setLoopMode(LoopMode.off);
        break;
    }
  }

  // ShuffleMode stuff
  final BehaviorSubject<bool> _shuffleModeBS = BehaviorSubject.seeded(false);
  Stream<bool> get shuffleMode$ => _shuffleModeBS.stream;
  bool get shuffleMode => _shuffleModeBS.value;

  Future<void> nextShuffleMode() async {
    bool shuffleMode = !_shuffleModeBS.value;
    _shuffleModeBS.add(shuffleMode);
    await _saveShuffleMode(shuffleMode);
  }

  void _shuffleTracks({Track? firstTrack}) {
    if (_tracksShuffled.isEmpty) return;

    _tracksShuffled.shuffle();
    firstTrack ??= track;

    if (firstTrack == null || !_tracksShuffled.contains(firstTrack)) return;
    if (_tracksShuffled.first != firstTrack) {
      int index = _tracksShuffled.indexOf(firstTrack);
      Track temp = _tracksShuffled[0];
      _tracksShuffled[0] = _tracksShuffled[index];
      _tracksShuffled[index] = temp;
    }
  }

  Future<void> _set(Track track) async {
    await _player.setFilePath(track.path);
    _track.add(track);
  }

  Future<void> _open(Track track) async {
    await _set(track);
    await _play();
  }

  Future<void> _play() async {
    track == null //
        ? await _playFromStop()
        : await _player.play();
    // if (_player.duration != null) {
    //   _player.seek(_player.duration! - const Duration(seconds: 2));
    // }
  }

  Future<void> _playFromStop() async {
    await _open(_tracksCSM.first);
  }

  // player functions
  Future<void> play(Track track) async {
    if (_tracksCSM.isEmpty) return;
    if (shuffleMode) _shuffleTracks(firstTrack: track);
    await _open(track);
  }

  Future<void> pause() async {
    await _player.pause();
  }

  Future<void> stop() async {
    if (track == null) return;
    _track.add(null);
    await _player.stop();
  }

  Future<void> playPause() async {
    if (_tracksCSM.isEmpty) return;
    if (track == null) {
      await _playFromStop();
      return;
    }

    isPlaying == true //
        ? await pause()
        : await _play();
  }

  Future<void> skipToNext() async {
    if (_tracksCSM.isEmpty) return;

    track != null //
        ? await _open(_nextTrackCSM)
        : await _open(_tracksCSM.first);
  }

  Future<void> skipToPrevious() async {
    if (_tracksCSM.isEmpty) return;

    track != null //
        ? await _open(_prevTrackCSM)
        : await _open(_tracksCSM.last);
  }

  Future<void> _onTrackCompleted() async {
    switch (loopMode) {
      case LoopMode.off:
        _isLastTrackCSM //
            ? await stop()
            : await _open(_nextTrackCSM);
        break;
      case LoopMode.one:
        await _open(track!);
        break;
      case LoopMode.all:
        if (shuffleMode && _isPenultTrackCSM) _shuffleTracks();
        await _open(_nextTrackCSM);
        break;
    }
  }

  Future<void> setTrackList(List<Track> newTracks) async {
    print('here');
    if (track != null && !newTracks.contains(track)) {
      Track? nextTrack = _findNextTrackExistingOnBothLists(newTracks);

      nextTrack != null
          ? isPlaying
              ? await _open(nextTrack)
              : await _set(nextTrack)
          : await stop();
    }

    _setTrackList([...newTracks]);
  }

  void _setTrackList(List<Track> newTracks) {
    _tracks.add([...newTracks]);
    _tracksShuffled = [...newTracks];
    if (shuffleMode) _shuffleTracks();
  }

  Track? _findNextTrackExistingOnBothLists(List<Track> newTracks) {
    int trackIndex = _tracksCSM.indexOf(track!);
    if (trackIndex == -1) return null;

    // from playing track to end of list
    for (int i = trackIndex; i < _tracksCSM.length; i++) {
      Track currentTrack = _tracksCSM[i];
      if (newTracks.contains(currentTrack)) {
        return currentTrack;
      }
    }

    if (loopMode != LoopMode.all) return null;

    // from begining of list to playing track
    for (int i = 0; i < trackIndex; i++) {
      Track currentTrack = _tracksCSM[i];
      if (newTracks.contains(currentTrack)) {
        return currentTrack;
      }
    }
  }

  Future<void> _loadLoopMode() async {
    String? loopModeString = await storageServise.getString('loopMode');

    switch (loopModeString) {
      case 'all':
        break;
      case 'one':
        _setLoopMode(LoopMode.one, save: false);
        break;
      case 'off':
        _setLoopMode(LoopMode.off, save: false);
        break;
    }
  }

  Future<void> _loadShuffleMode() async {
    bool shuffleMode = await storageServise.getBool('shuffleMode') ?? false;
    _shuffleModeBS.add(shuffleMode);
  }

  Future<bool> _saveLoopMode(String loopModeString) async {
    return await storageServise.setString('loopMode', loopModeString);
  }

  Future<bool> _saveShuffleMode(bool shuffleMode) async {
    return await storageServise.setBool('shuffleMode', shuffleMode);
  }
}
