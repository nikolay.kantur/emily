import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class StorageService {
  static final StorageService _singleton = StorageService._internal();

  factory StorageService() {
    return _singleton;
  }

  late SharedPreferences prefs;
  final Completer<void> _initComplete = Completer();

  StorageService._internal() {
    _init();
  }

  _init() async {
    prefs = await SharedPreferences.getInstance();
    _initComplete.complete();
  }

  Future<bool> setString(String key, String value) {
    return prefs.setString(key, value);
  }

  Future<String?> getString(String key) async {
    await _initComplete.future;
    return prefs.getString(key);
  }

  Future<bool> setStringList(String key, List<String> value) {
    return prefs.setStringList(key, value);
  }

  Future<List<String>?> getStringList(String key) async {
    await _initComplete.future;
    return prefs.getStringList(key);
  }

  Future<bool> setBool(String key, bool value) async {
    return prefs.setBool(key, value);
  }

  Future<bool?> getBool(String key) async {
    await _initComplete.future;
    return prefs.getBool(key);
  }
}
