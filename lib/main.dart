import 'package:flutter/material.dart';
import 'package:emily/pages/playlist/playlist.page.dart';

void main() {
  runApp(const MusicApp());
}

class MusicApp extends StatelessWidget {
  const MusicApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Multisync Music app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const PlaylistPage(),
    );
  }
}
