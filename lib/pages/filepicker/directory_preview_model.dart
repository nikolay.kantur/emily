import 'package:emily/pages/filepicker/file_preview_model.dart';

class DirectoryPreview {
  final String path;
  final List<FilePreview> fileList;
  final String? root;
  final String? error;

  DirectoryPreview({
    required this.path,
    required this.fileList,
    this.root,
    this.error,
  });
}
