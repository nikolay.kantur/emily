import 'package:emily/pages/filepicker/file_preview_model.dart';
import 'package:flutter/material.dart';

class FilePreviewExtended extends FilePreview {
  IconData icon;
  bool selected;
  FilePreviewExtended({
    filename,
    path,
    extension,
    type,
    required this.icon,
    this.selected = false,
  }) : super(
          filename: filename,
          path: path,
          extension: extension,
          type: type,
        );

  static FilePreviewExtended fromFilePreview(FilePreview file) =>
      FilePreviewExtended(
        type: file.type,
        filename: file.filename,
        path: file.path,
        icon: _getFileIcon(file.type),
      );

  static IconData _getFileIcon(FileType type) {
    switch (type) {
      case FileType.back:
        return Icons.folder_outlined;
      case FileType.directory:
        return Icons.folder_outlined;
      case FileType.file:
        return Icons.description_outlined;
      case FileType.internal:
        return Icons.storage;
      case FileType.sdCard:
        return Icons.sd_card;
    }
  }
}
