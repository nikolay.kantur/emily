import 'package:emily/pages/filepicker/directory_preview_model.dart';
import 'package:emily/pages/filepicker/file_preview_extended_model.dart';
import 'package:flutter/material.dart';
import 'package:emily/services/file.service.dart';
import 'package:emily/services/storage.service.dart';

import 'file_preview_model.dart';

class FilePickerPage extends StatefulWidget {
  const FilePickerPage({Key? key}) : super(key: key);

  @override
  State<FilePickerPage> createState() => _MainPageState();
}

class _MainPageState extends State<FilePickerPage> {
  final StorageService storageService = StorageService();
  final fileService = FileService();

  String path = ''; // ? just nullable
  String? root;
  List<FilePreviewExtended> fileList = [];
  int get selectedCount => fileList.where((file) => file.selected).length;
  int listLenght = 0;

  @override
  initState() {
    super.initState();
    init();
  }

  init() async {
    await _loadPath();
    await _getFileList(path);
  }

  Future<void> _loadPath() async {
    path = await storageService.getString('filepicker_path') ?? '';
  }

  Future<void> _savePath() async {
    await storageService.setString('filepicker_path', path);
  }

  _getFileList(String path) async {
    DirectoryPreview dirPreview = await fileService.getDirectoryPreview(
      path,
      extensions: ['.mp3'],
    );

    if (dirPreview.error != null) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(dirPreview.error!)));
    }

    root = dirPreview.root;
    print('root: $root');

    fileList =
        dirPreview.fileList.map(FilePreviewExtended.fromFilePreview).toList();
    listLenght = fileList
        .where((file) => file.type != FileType.back)
        .length; // Перенести в get?
    this.path = dirPreview.path;
    await _savePath();
    setState(() {});
  }

  _toggleSelect(FilePreviewExtended file) {
    if (file.type == FileType.back) return;
    file.selected = !file.selected;
    setState(() {});
  }

  _onLineTap(file) {
    file.type == FileType.file
        ? _toggleSelect(file)
        : selectedCount == 0
            ? _getFileList(file.path)
            : _toggleSelect(file);
  }

  _selectAll() {
    for (FilePreviewExtended file in fileList) {
      if (file.filename == '..') continue;
      file.selected = true;
    }
    setState(() {});
  }

  _deselectAll() {
    for (FilePreviewExtended file in fileList) {
      if (file.filename == '..') continue;
      file.selected = false;
    }
    setState(() {});
  }

  _toggleAll() => selectedCount != listLenght //
      ? _selectAll()
      : _deselectAll();

  void _returnSelectedData() async {
    List<String> filePathList = [];
    for (FilePreviewExtended file in fileList) {
      if (file.selected != true) continue;
      if (file.type == FileType.file) {
        filePathList.add(file.path);
        continue;
      }
      if (file.type == FileType.directory) {
        DirectoryPreview dirPreview = await fileService.getDirectoryPreview(
          file.path,
          recursive: true,
          extensions: ['.mp3'],
          onlyFiles: true,
        );
        filePathList.addAll(dirPreview.fileList.map((file) => file.path));
      }
    }
    Navigator.pop(context, filePathList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: Navigator.of(context).pop,
            icon: const Icon(Icons.arrow_back),
            color: Colors.white,
            // iconSize: 32,
            splashRadius: 28,
          ),
          title: _titleWidget(root ?? ''),
          actions: [
            _selectAllToggleButton(),
            _doneSelection(),
          ],
        ),
        body: ListView(
          children: fileList
              .map(
                (el) => Stack(
                  children: [
                    Container(
                      padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
                      color: el.selected ? Colors.grey : Colors.white,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(el.selected ? Icons.done : el.icon),
                          const SizedBox(width: 5),
                          Expanded(
                            child: Text(
                              el.filename,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(fontSize: 20),
                              maxLines: 3,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned.fill(
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => _onLineTap(el),
                          onLongPress: () => _toggleSelect(el),
                        ),
                      ),
                    ),
                  ],
                ),
              )
              .toList(),
        ));
  }

  IconButton _doneSelection() => IconButton(
        icon: const Icon(Icons.done),
        tooltip: 'Select files',
        onPressed: _returnSelectedData,
        // iconSize: 32,
        splashRadius: 28,
      );

  IconButton _selectAllToggleButton() => IconButton(
        icon: const Icon(Icons.select_all),
        tooltip: 'Select all',
        onPressed: _toggleAll,
        // iconSize: 32,
        splashRadius: 28,
      );

  StatelessWidget _titleWidget([String root = '']) => //
      selectedCount != 0
          ? Text('Выбрано: $selectedCount/$listLenght')
          : _PathLineWithSelectors(
              parts: path
                  .replaceFirst(root, '')
                  .split('/')
                  .where((part) => part != '')
                  .toList()
                  .asMap(),
              getFileList: _getFileList,
              root: root,
            );
}

class _PathLineWithSelectors extends StatelessWidget {
  final Map<int, String> parts;
  final Function getFileList;
  final String? root;
  const _PathLineWithSelectors({
    Key? key,
    required this.parts,
    required this.getFileList,
    required this.root,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        reverse: true,
        child: Row(
          children: [
            root == ''
                ? Container()
                : InkWell(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: root == FileService.defaultPath
                          ? const Icon(Icons.storage)
                          : const Icon(Icons.sd_card),
                    ),
                    onTap: () => getFileList(root),
                  ),
            ...parts.entries.map((entry) => Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 1),
                      child: Text('/'),
                    ),
                    InkWell(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(entry.value),
                      ),
                      onTap: () {
                        String newPath = parts.entries
                            .where((e) => e.key <= entry.key)
                            .map((e) => e.value)
                            .join('/');
                        print('newPath: $newPath');
                        getFileList('$root/$newPath');
                      },
                    ),
                  ],
                ))
          ],
        ));
  }
}
