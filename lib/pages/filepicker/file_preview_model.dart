enum FileType { file, directory, back, internal, sdCard }

class FilePreview {
  FileType type;
  String filename;
  String? extension;
  String path;

  FilePreview({
    required this.filename,
    required this.path,
    required this.type,
    this.extension,
  });
}
