import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:flutter/material.dart';
import 'package:emily/panels/player.panel.dart';
import 'package:emily/resources/resources.dart';
import 'package:emily/services/player.service.dart';
import 'package:rxdart/rxdart.dart';

class PlayerPage extends StatefulWidget {
  const PlayerPage({Key? key}) : super(key: key);

  @override
  _PlayerPageState createState() => _PlayerPageState();
}

class _PlayerPageState extends State<PlayerPage> {
  final PlayerService playerService = PlayerService();
  final PublishSubject<void> unsubscriber = PublishSubject<void>();

  Duration duration = const Duration();
  Duration position = const Duration();

  @override
  void initState() {
    playerService.track$.takeUntil(unsubscriber).listen((_) {
      setState(() {});
    });

    playerService.duration$
        .takeUntil(unsubscriber)
        .listen((Duration? duration) {
      if (duration == null) return;
      this.duration = duration;
      setState(() {});
    });

    playerService.position$.takeUntil(unsubscriber).listen((Duration position) {
      this.position = position;
      setState(() {});
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(children: [
        SizedBox(
          height: MediaQuery.of(context).size.width,
          width: MediaQuery.of(context).size.width,
          child: playerService.track?.cover != null
              ? Image.memory(
                  playerService.track!.cover!,
                  fit: BoxFit.cover,
                )
              : const Image(
                  image: AssetImage(AppImages.noCover),
                  fit: BoxFit.cover,
                ),
        ),
        TrackNameWidget(
          track: playerService.track,
          crossAxisAlignment: CrossAxisAlignment.center,
          maxLines: 3,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: ProgressBar(
            progress: position,
            total: duration,
            progressBarColor: Colors.blue,
            baseBarColor: Colors.white70,
            onSeek: playerService.setDuration,
            timeLabelLocation: TimeLabelLocation.sides,
            barHeight: 3,
          ),
        ),
      ]),
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue,
        child: SizedBox(
          height: 64,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Row(
              children: [
                const PlayerPageButton(),
                const Expanded(child: SizedBox(width: double.infinity)),
                SkipToPrevButton(),
                SkipToNextButton(),
                const Expanded(child: SizedBox(width: double.infinity)),
                NextShuffleModeButton(),
                LoopModeButton(),
                const Expanded(child: SizedBox(width: double.infinity)),
                PlayPauseButton(),
                StopButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PlayerPageButton extends StatelessWidget {
  const PlayerPageButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => Navigator.pop(context),
      icon: const Icon(Icons.list),
      iconSize: 32,
      splashRadius: 28,
    );
  }
}
