import 'package:flutter/material.dart';
import 'package:emily/classes/track.class.dart';
import 'package:emily/pages/filepicker/filepicker.page.dart';
import 'package:emily/services/player.service.dart';
import 'package:emily/services/storage.service.dart';
import 'package:rxdart/rxdart.dart';

class PlaylistService {
  final PlayerService playerService = PlayerService();
  final StorageService storageService = StorageService();

  PlaylistService() {
    init();
  }

  void init() async {
    _loadPlaylist();
  }

  final BehaviorSubject<List<Track>> _tracks = BehaviorSubject.seeded([]);
  Stream<List<Track>> get tracks$ => _tracks.stream;
  List<Track> get tracks => _tracks.value;

  Future<void> _loadPlaylist() async {
    List<String> pathList = await storageService.getStringList('tracks') ?? [];

    await addToEndByPathList(pathList, save: false);
  }

  Future<void> addToEndByPathList(
    List<String> pathList, {
    bool save = true,
  }) async {
    pathList = pathList.where((path) {
      return _tracks.value.where((track) => track.path == path).isEmpty;
    }).toList();

    for (String path in pathList) {
      Track track = Track(path);
      await track.parseTrack();
      _tracks.add([..._tracks.value, track]);
    }

    if (save) {
      await _savePlaylist();
    }

    await playerService.setTrackList(tracks);
  }

  Future<void> removeByTrack(Track track) async {
    _tracks.value.remove(track);
    _tracks.add(_tracks.value);
    await _savePlaylist();
    await playerService.setTrackList(tracks);
  }

  Future<void> _savePlaylist() async {
    await storageService.setStringList(
      'tracks',
      _tracks.value.map((track) => track.path).toList(),
    );
  }

  Future<void> removeAll() async {
    _tracks.add([]);
    await _savePlaylist();
    await playerService.setTrackList(tracks);
  }

  Future<void> tapOnTrack(Track track) async {
    track == playerService.track
        ? await playerService.playPause()
        : await playerService.play(track);
  }

  void addFiles(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (_) => const FilePickerPage()),
    ).then((list) {
      if (list is! List<String>) return;
      if (list.isEmpty) return;
      addToEndByPathList(list);
    });
  }

  void shuffle() async {
    List<Track> tracks = _tracks.value;
    tracks.shuffle();
    _tracks.add(tracks);

    await playerService.setTrackList(tracks);
    await _savePlaylist();
  }
}
