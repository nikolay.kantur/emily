import 'package:flutter/material.dart';
import 'package:emily/classes/track.class.dart';
import 'package:emily/pages/playlist/playlist.popup_menu.dart';
import 'package:emily/pages/playlist/playlist.service.dart';
import 'package:emily/helpers/to_string_duration.dart';
import 'package:emily/helpers/to_string_human_readable_typed.dart';
import 'package:emily/services/player.service.dart';
import 'package:emily/panels/player.panel.dart';
import 'package:rxdart/rxdart.dart';

const TextStyle firstRowStyle = TextStyle(fontSize: 18);

class PlaylistPage extends StatefulWidget {
  const PlaylistPage({Key? key}) : super(key: key);

  @override
  State<PlaylistPage> createState() => _PlaylistPageState();
}

class _PlaylistPageState extends State<PlaylistPage> {
  final String title = 'Playlist';
  final PublishSubject<void> unsubscriber = PublishSubject<void>();

  final PlaylistService playlistService = PlaylistService();
  final PlayerService playerService = PlayerService();

  @override
  void initState() {
    super.initState();

    playlistService.tracks$
        .takeUntil(unsubscriber)
        .listen((List<Track> tracks) {
      setState(() {});
    });

    playerService.playerState$.takeUntil(unsubscriber).listen((_) {
      setState(() {});
    });

    playerService.track$.takeUntil(unsubscriber).listen((_) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: [
          IconButton(
            onPressed: () => playlistService.addFiles(context),
            icon: const Icon(Icons.add),
            // iconSize: 32,
            splashRadius: 28,
          ),
          PlaylistPopupMenu(playlistService: playlistService),
        ],
      ),
      body: ListView.builder(
        itemCount: playlistService.tracks.length,
        itemBuilder: (context, index) => _PlaylistRow(
          track: playlistService.tracks[index],
          index: ++index,
          playlistService: playlistService,
          playerService: playerService,
        ),
      ),
      bottomNavigationBar: const PlayerPanel(),
      // playerService.hasTrack ? const PlayerPanel() : null,
    );
  }

  @override
  dispose() {
    unsubscriber.add(null);
    super.dispose();
  }
}

class _PlaylistRow extends StatefulWidget {
  final Track track;
  final int index;
  final PlaylistService playlistService;
  final PlayerService playerService;

  const _PlaylistRow({
    required this.index,
    required this.playlistService,
    required this.playerService,
    required this.track,
  });

  @override
  State<_PlaylistRow> createState() => _PlaylistRowState();
}

class _PlaylistRowState extends State<_PlaylistRow> {
  @override
  void initState() {
    super.initState();
  }

  RelativeRect _getPosition() {
    RenderBox box = context.findRenderObject()! as RenderBox;
    var offset = box.localToGlobal(Offset.zero);
    return RelativeRect.fromLTRB(
      box.size.width + offset.dx,
      box.size.height + offset.dy,
      box.size.width + offset.dx,
      box.size.height + offset.dy,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: widget.track == widget.playerService.track
              ? Colors.black12
              : Colors.white,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(3, 5, 3, 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      '${widget.index}. ',
                      style: firstRowStyle,
                    ),
                    Expanded(
                      child: Text(
                        widget.track.name,
                        style: firstRowStyle,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Text(
                      toStringDuration(widget.track.duration),
                      style: firstRowStyle,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 3,
                ),
                Text(
                    [
                      widget.track.extension.toString().toUpperCase(),
                      toStringHumanReadableTyped(widget.track.sampleRate,
                          type: 'Hz'),
                      toStringHumanReadableTyped(widget.track.bitrate,
                          type: 'bps'),
                      widget.track.channelMode,
                      toStringHumanReadableTyped(widget.track.size, type: 'B'),
                    ].where((str) => str != null && str != '').join(' :: '),
                    style: const TextStyle(
                      fontSize: 15,
                      color: Colors.black38,
                    )),
              ],
            ),
          ),
        ),
        Positioned.fill(
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => widget.playlistService.tapOnTrack(widget.track),
              onLongPress: () => showMenu(
                context: context,
                position: _getPosition(),
                items: [
                  PopupMenuItem(
                    // onTap: () {
                    //   widget.playlistService.removeByTrack(widget.track);
                    // },
                    child: Row(
                      children: const [
                        Icon(Icons.delete, color: Colors.black),
                        SizedBox(width: 15),
                        Text('Delete track'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
