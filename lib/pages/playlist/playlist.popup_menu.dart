import 'package:flutter/material.dart';
import 'package:emily/pages/playlist/playlist.service.dart';

class PlaylistPopupMenu extends StatelessWidget {
  const PlaylistPopupMenu({
    Key? key,
    required this.playlistService,
  }) : super(key: key);

  final PlaylistService playlistService;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      onSelected: (PlaylistMenuOption value) {
        switch (value) {
          // case PlaylistMenuOption.add:
          //   playlistService.addFiles(context);
          //   break;
          case PlaylistMenuOption.shuffle:
            playlistService.shuffle();
            break;
          case PlaylistMenuOption.delete:
            showDialog<String>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Delete all tracks'),
                content: const Text('This will remove all tracks'),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Text('Cancel'),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context, 'OK');
                      playlistService.removeAll();
                    },
                    child:
                        const Text('OK', style: TextStyle(color: Colors.red)),
                  ),
                ],
              ),
            );
            break;
        }
      },
      itemBuilder: (BuildContext context) => [
        // PopupMenuItem(
        //   value: PlaylistMenuOption.add,
        //   child: Row(
        //     children: const [
        //       Icon(Icons.add, color: Colors.black),
        //       SizedBox(width: 15),
        //       Text('Add'),
        //     ],
        //   ),
        // ),
        PopupMenuItem(
          value: PlaylistMenuOption.shuffle,
          child: Row(
            children: const [
              Icon(Icons.shuffle, color: Colors.black),
              SizedBox(width: 15),
              Text('Shuffle playlist'),
            ],
          ),
        ),
        PopupMenuItem(
          value: PlaylistMenuOption.delete,
          child: Row(
            children: const [
              Icon(Icons.delete, color: Colors.black),
              SizedBox(width: 15),
              Text('Delete all'),
            ],
          ),
        ),
      ],
    );
  }
}

enum PlaylistMenuOption {
  delete,
  // add,
  shuffle,
}
