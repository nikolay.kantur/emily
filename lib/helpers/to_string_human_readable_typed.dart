import 'dart:math';

String _getPrefixByPower(int power) {
  Map<int, String> _prefixByPower = {
    -24: 'y', // yocto
    -21: 'z', // zepto
    -18: 'a', // atto
    -15: 'f', // femto
    -12: 'p', // pico
    -9: 'n', // nano
    -6: 'μ', // micro
    -3: 'm', // milli
    0: '', //
    3: 'k', // kilo
    6: 'M', // mega
    9: 'G', // giga
    12: 'T', // tera
    15: 'P', // peta
    18: 'E', // exa
    21: 'Z', // zetta
    24: 'Y', // yotta
  };

  return _prefixByPower[power] ?? '';
}

_applyPrefixAndType(
  String val, {
  String? type,
  String? prefix,
  required bool? noSpaces,
}) {
  prefix = prefix ?? '';

  /// если тип не указан, то преобразует в строку как есть
  if (type == '' || type == null) {
    return '$val$prefix';
  }

  /// иначе отдаёт с типом через пробел
  if (noSpaces == true) {
    return '$val$prefix$type';
  }
  return '$val $prefix$type';
}

int _getSign(num val) {
  return val < 0 ? -1 : 1;
}

String toStringHumanReadableTyped(
  num? val, {
  String? type, // can be B bites
  int? precision,
  int? toPow,
  bool? noSpaces,
}) {
  if (val == null) {
    return '';
  }
  final runtimeType = val.runtimeType;
  int power =
      toPow ?? int.parse(val.toStringAsExponential(0).split('e').toList().last);
  int powerSign = _getSign(power);

  // числа 0 - 1000
  if (power < 3 && power >= 0) {
    precision = precision ?? (runtimeType == int ? 0 : 1);
    return _applyPrefixAndType(val.toStringAsFixed(precision),
        type: type, noSpaces: noSpaces);
  }

  int power3 = (power / 3).truncate() * 3;
  String prefix = _getPrefixByPower(power3);
  double valPowered = val / pow(10, power3);

  /// если точность указана, то всегда выводим с ней
  if (precision != null) {
    return _applyPrefixAndType(valPowered.toStringAsFixed(precision),
        type: type, prefix: prefix, noSpaces: noSpaces);
  }

  // precision == null
  if (runtimeType == double && powerSign == 1) {
    return _applyPrefixAndType(valPowered.toStringAsFixed(1),
        type: type, prefix: prefix, noSpaces: noSpaces);
  }

  num whole = valPowered.truncate();
  String? fraction = _getFraction(valPowered);

  if (whole == 0 && fraction != null && fraction.length > 1) {
    valPowered *= 1000;
    power3 -= 3;
    prefix = _getPrefixByPower(power3).toString();
    fraction = _getFraction(valPowered);
  }
  if (fraction == '0' || fraction == null) {
    return _applyPrefixAndType(valPowered.toStringAsFixed(0),
        type: type, prefix: prefix, noSpaces: noSpaces);
  }
  return _applyPrefixAndType(valPowered.toStringAsFixed(1),
      type: type, prefix: prefix, noSpaces: noSpaces);
}

String? _getFraction(num valPowered) {
  final List<String> parts = valPowered.toString().split('.');
  final String? fraction = parts.length > 1 ? parts[1] : null;

  return fraction;
}
