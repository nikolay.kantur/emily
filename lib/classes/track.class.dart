import 'dart:io';
import 'dart:typed_data';

import 'package:flutter_media_metadata/flutter_media_metadata.dart';
import 'package:mime/mime.dart';
import 'package:path/path.dart' as p;

class Track {
  String path;
  late String basename;
  late String extension;

  static final MetadataRetriever metadataRetriever = MetadataRetriever();

  int? size; // File
  String? mimeType; // Mime

  String? artist; // MetadataRetriever
  String? title; // MetadataRetriever
  Duration? duration; // mp3 // MetadataRetriever
  int? sampleRate; // mp3
  int? bitrate; // mp3
  ChannelMode? channelMode; // mp3
  Uint8List? cover; // MetadataRetriever

  Track(
    this.path, {
    this.size,
    this.mimeType,
    this.artist,
    this.title,
    this.duration,
    this.sampleRate,
    this.bitrate,
    this.channelMode,
    this.cover,
  }) {
    final String basename = p.basename(path);
    this.basename =
        basename.substring(0, basename.length - p.extension(path).length);
    String extension = p.extension(path);
    this.extension = extension.isNotEmpty ? extension.substring(1) : extension;
  }

  String get name {
    if (artist != null) {
      return (title != null) ? '${artist!} - ${title!}' : artist!;
    } else {
      return title != null ? title! : basename;
    }
  }

  bool get hasArtistAndTitle => (artist != null || title != null);

  @override
  String toString() {
    return path;
  }

  Future<void> parseTrack() async {
    final File file = File(path);

    size = file.lengthSync();
    mimeType = lookupMimeType(path);

    await metadataRetriever.setFile(file);
    Metadata metadata = await metadataRetriever.metadata;
    // print('metadata: ${metadata.toMap()}');
    artist = metadata.trackArtistNames?.join(', ');
    title = metadata.trackName;
    if (metadata.trackDuration != null) {
      duration = Duration(milliseconds: metadata.trackDuration!);
    }

    cover = metadataRetriever.albumArt;

    // track.sampleRate = 44000;
    // track.bitrate = 320000;
    // track.channelMode = ChannelMode.stereo;
  }
}

enum ChannelMode {
  stereo,
  jointStereo,
  dualChannel,
  singleChannel,
}
