import 'package:flutter_test/flutter_test.dart';
import 'package:emily/helpers/to_string_human_readable_typed.dart';

void main() {
  const f = toStringHumanReadableTyped;
  group('int\'s', () {
    test('should not have fractional part equal to 0', () {
      expect(f(100000000000, type: 'Hz'), '100 GHz');
      expect(f(10000000000, type: 'Hz'), '10 GHz');
      expect(f(1000000000, type: 'Hz'), '1 GHz');
      expect(f(100000000, type: 'Hz'), '100 MHz');
      expect(f(10000000, type: 'Hz'), '10 MHz');
      expect(f(1000000, type: 'Hz'), '1 MHz');
      expect(f(100000, type: 'Hz'), '100 kHz');
      expect(f(10000, type: 'Hz'), '10 kHz');
      expect(f(1000, type: 'Hz'), '1 kHz');
      expect(f(100, type: 'Hz'), '100 Hz');
      expect(f(10, type: 'Hz'), '10 Hz');
      expect(f(1, type: 'Hz'), '1 Hz');
      expect(f(0, type: 'Hz'), '0 Hz');
    });

    test('fraction for big ints', () {
      expect(f(100100000000, type: 'Hz'), '100.1 GHz');
      expect(f(101300000000, type: 'Hz'), '101.3 GHz');
    });
    test('fraction for big ints, less then .045', () {
      expect(f(100010000000, type: 'Hz'), '100.0 GHz');
      expect(f(100001000000, type: 'Hz'), '100.0 GHz');
      expect(f(100000100000, type: 'Hz'), '100.0 GHz');
    });
    test('rounding fraction', () {
      expect(f(100060000000, type: 'Hz'), '100.1 GHz');
      expect(f(100999999999, type: 'Hz'), '101.0 GHz');
      // expect(f(100050000000, type: 'Hz'), '100.1 GHz'); // toStringAsFixed has bug
      expect(f(123456789012, type: 'Hz'), '123.5 GHz');
      expect(f(12345, type: 'Hz'), '12.3 kHz');
      expect(f(1234, type: 'Hz'), '1.2 kHz');
      expect(f(1555, type: 'Hz'), '1.6 kHz');
      expect(f(1999, type: 'Hz'), '2.0 kHz');
    });
  });

  group('doubles', () {
    test('double: should have .0 even if fraction equals to 0', () {
      expect(f(100000000000.0, type: 'Hz'), '100.0 GHz');
      expect(f(1000000000.0, type: 'Hz'), '1.0 GHz');
      expect(f(1000.0, type: 'Hz'), '1.0 kHz');
      expect(f(100.0, type: 'Hz'), '100.0 Hz');
      expect(f(10.0, type: 'Hz'), '10.0 Hz');
      expect(f(1.0, type: 'Hz'), '1.0 Hz');
      expect(f(0.0, type: 'Hz'), '0.0 Hz');
    });
    test('double: rounding', () {
      expect(f(10.1, type: 'Hz'), '10.1 Hz');
      expect(f(10.05, type: 'Hz'), '10.1 Hz');
      expect(f(10.12, type: 'Hz'), '10.1 Hz');
      expect(f(10.1499999, type: 'Hz'), '10.1 Hz');
      expect(f(10.15, type: 'Hz'), '10.2 Hz');
    });

    test('less then 1', () {
      expect(f(0.1, type: 'Hz'), '0.1 Hz');
      expect(f(0.12, type: 'Hz'), '120 mHz');
      expect(f(0.123, type: 'Hz'), '123 mHz');
      expect(f(0.1234, type: 'Hz'), '123.4 mHz');
      expect(f(0.12345, type: 'Hz'), '123.5 mHz');
      expect(f(0.0000012, type: 'Hz'), '1.2 μHz');
      expect(f(0.000000123, type: 'Hz'), '123.0 nHz');
      expect(f(0.00000001234, type: 'Hz'), '12.3 nHz');
      expect(f(0.0000000012345, type: 'Hz'), '1.2 nHz');
    });
  });
  group('etc', () {
    test('negative', () {
      expect(f(-0.0, type: 'Hz'), '-0.0 Hz');
      expect(f(-0, type: 'Hz'), '0 Hz');
      expect(f(-0.12345, type: 'Hz'), '-123.5 mHz');
    });

    test('precision', () {
      expect(f(100000000000, type: 'Hz', precision: 1), '100.0 GHz');
      expect(f(100000000000.0, type: 'Hz', precision: 1), '100.0 GHz');
      expect(f(100000000000.0, type: 'Hz', precision: 2), '100.00 GHz');
    });

    test('toPow', () {
      expect(f(100000000000.0, type: 'Hz', precision: 2, toPow: 3),
          '100000000.00 kHz');
      expect(f(10, type: 'Hz', precision: 1, toPow: 3), '0.0 kHz');
      expect(f(10.0, type: 'Hz', precision: 1, toPow: 3), '0.0 kHz');
      expect(f(10.0, type: 'Hz', precision: 2, toPow: 3), '0.01 kHz');
      expect(f(1.0, type: 'Hz', precision: 2, toPow: 3), '0.00 kHz');
    });
    test('noSpaces', () {
      expect(f(-0.12345, type: 'Hz', noSpaces: true), '-123.5mHz');
    });
  });
}
