import 'dart:io';

import 'package:emily/resources/resources.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('images assets test', () {
    expect(true, File(AppImages.noCover).existsSync());
  });
}
