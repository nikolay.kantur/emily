import 'package:flutter_test/flutter_test.dart';
import 'package:emily/helpers/to_string_duration.dart';

void main() {
  const f = toStringDuration;
  test('zero', () {
    expect(f(const Duration(milliseconds: 1)), '00:00');
    expect(f(const Duration(seconds: 0)), '00:00');
  });
  test('only seconds', () {
    expect(f(const Duration(seconds: 5)), '00:05');
  });
  test('minutes and seconds', () {
    expect(f(const Duration(minutes: 3, seconds: 5)), '03:05');
  });
  test('hours, minutes and seconds', () {
    expect(f(const Duration(hours: 05, minutes: 3, seconds: 5)), '5:03:05');
    expect(f(const Duration(hours: 15, minutes: 3, seconds: 5)), '15:03:05');
  });
  test('hours more then 24', () {
    expect(f(const Duration(hours: 115, minutes: 3, seconds: 5)), '115:03:05');
  });
}
